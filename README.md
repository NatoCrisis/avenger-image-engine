# avenger-image-engine
To build the To build the project locally
- sam build

To run the project locally
- sam local start-api

Function to write text over an image.
Images come from a Lambda layer, the character is specified and the engine will select an image at random

Sample request
```
{
    "character": "ironman",
    "text": "You told me to digest when you’re telling me he liked me, so it’s a zoo out there, watch out.Hey, nice to see you in your room."
}
```

The function returns a Base 64 encoded string representing the image, which must be decoded
```
byte_data = base64.b64decode(resp_body)
image_data = BytesIO(byte_data)
img = Image.open(image_data)

img.save('new-image.jpg')
```
