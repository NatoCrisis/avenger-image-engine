from pathlib import Path


def validate(msg_body):
    error_list = []
    is_valid = True

    character = msg_body.get('character')
    text = msg_body.get('text')

    if not character:
        error_list.append(f'Missing parameter: character')
        is_valid = False
    elif not validate_character(character):
        error_list.append(f'Invalid character: {character}')
        is_valid = False

    if not text:
        error_list.append(f'Missing parameter: text')
        is_valid = False

    return error_list, is_valid, (character, text)


def validate_character(character):
    return Path('/opt/python/' + character).is_dir()
