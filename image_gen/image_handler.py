import io

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


class ImageHandler:

    def __init__(self, file_name):
        font = ImageFont.truetype("/opt/python/OpenSans-Regular.ttf", 35)
        img = Image.open(file_name)
        draw = ImageDraw.Draw(img)

        self.font = font
        self.img = img
        self.draw = draw

    def write_text(self, to_write):
        if to_write is None:
            return self

        # Split the text, to write over multiple lines
        # Sweet spot is around 40 characters
        lines = self.split_text(to_write)

        # Arbitrary starting point for the text. The size of the image is
        # known right now, will probably try to calculate this in the future
        idx = 250
        for line in lines:
            # stroke width/fill gives text a black outline. Shows up better on images
            self.draw.text((55, idx), line, (255, 255, 255), font=self.font, stroke_width=2, stroke_fill='black')
            # Next line is height of the font plus 5 for padding
            idx += 40

        return self

    def as_byte_array(self):
        buffr = io.BytesIO()

        self.img.save(buffr, format='JPEG')
        self.img.close()

        return buffr.getvalue()

    def split_text(self, to_split, sentence_len=40):
        all_words = to_split.split(' ')
        to_return = []
        curr_line = ''

        while len(all_words) > 0:
            curr_line += all_words.pop(0) + ' '
            if len(curr_line) >= sentence_len:
                to_return.append(curr_line)
                curr_line = ''

        # In case the last list of words was less than 40, make sure to append it
        if curr_line:
            to_return.append(curr_line)

        return to_return
