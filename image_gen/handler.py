import json
import base64

from validator import validate
from file_picker import random_image
from image_handler import ImageHandler


def process_post(event):
    body = json.loads(event.get('body'))

    error_list, is_valid, values = validate(body)

    return_body = None
    if is_valid:
        return_body = get_image(values)
    else:
        return_body = json.dumps(
            {'message': 'The following errors were found', 'errorList': error_list})

    return {
        "headers": {"Content-Type": "image/jpg" if is_valid else "application/json"},
        "statusCode": 200 if is_valid else 400,
        "body": return_body,
        "isBase64Encoded": is_valid
    }


def get_image(values):
    character, txt = values
    file_name = random_image(character)
    byte_img = ImageHandler(file_name).write_text(txt).as_byte_array()
    return base64.b64encode(byte_img).decode('utf-8')

