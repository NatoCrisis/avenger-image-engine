from handler import process_post


def lambda_handler(event, context):
    return process_post(event)
